# Check SOA

## Program

`check_soa.py` is a simple program to query the SOA record from all nameservers (or from a list of servers) for a given zone and display the serial number.

Options allow a JSON output or a Nagios-compatible output, TCP mode, DNS over TLS mode (with optionnal certificate verification)... See [usage](#usage) for all options

When giving a list of servers, enable recursion if given servers are recursive servers

## Requirements

- Python ≥ 3.10
- [dnspython](https://www.dnspython.org/) ≥ 2.3.0

## Usage

The possible options are:

- -h, --help: show this very message
- -4, --ipv4: Use IPv4 only
- -6, --ipv6: Use IPv6 only
- --multi: Use multi-threading for faster results
- --tcp: Use TCP
- --tls: Use DNS over TLS
- --tls-verify: Verify certificate when in DoT mode
- --ca-cert `/PATH/TO/CERTSTORE` Path to a CA certificate or a directory containing the certificates. Default: `/etc/ssl/certs/`
- --time: Add the response time of queries (in ms)
- --nodnssec: Disable DNSSEC validation for the requests to get the domain's NS
- --noedns: Disable EDNS
- --nsid: Enable NSID EDNS option
- -m, --monitor: Nagios compatible output
- -n, --no-check: Disable serial check. If servers return different serials, no error is raised
- --json: JSON output
- -w `WARNING`, --warning `WARNING`: Warning threshold (monitor mode)
- -c `CRITICAL`, --critical `CRITICAL`: Critical threshold (monitor mode)
- -t `FLOAT`: Timeout (in seconds). Default: 1.5
- --ns `SERVERS`: Name servers to query (comma-separated list)
- -r, --recursion: Enable RD bit in queries. Mandatory if recursive servers are given with --ns

Some notes:

- Serials are by default compared:
  - In monitoring mode, if multiple serials are spotted `check_soa.py` will consider the highest value as the legitimate one and raise a `WARNING` or `CRITICAL` state.
  - In regular mode, a warning will be added.
  - You can disable this behavior by using the `--no-check` option.
  - JSON output does not compare serials yet.

## LICENSE

GNU GPLv3
